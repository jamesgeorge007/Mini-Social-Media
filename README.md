[![Build Status](https://travis-ci.org/jamesgeorge007/Mini-Social-Media.svg?branch=master)](https://travis-ci.org/jamesgeorge007/Mini-Social-Media) [![GitHub issues](https://img.shields.io/github/issues/jamesgeorge007/Mini-Social-Media.svg)](https://github.com/jamesgeorge007/Mini-Social-Media/issues) [![GitHub forks](https://img.shields.io/github/forks/jamesgeorge007/Mini-Social-Media.svg)](https://github.com/jamesgeorge007/Mini-Social-Media/network) [![GitHub stars](https://img.shields.io/github/stars/jamesgeorge007/Mini-Social-Media.svg)](https://github.com/jamesgeorge007/Mini-Social-Media/stargazers) [![GitHub license](https://img.shields.io/github/license/jamesgeorge007/Mini-Social-Media.svg)](https://github.com/jamesgeorge007/Mini-Social-Media/blob/master/LICENSE)

<h1 align="center"> Mini-Social-Media </h1>

**Basic Info**

This is a basic social media web app where the users can create their accounts connect with others and post their activities. This is backed up by PHP with MySQL as the database while the front end part is designed in HTML 5, CSS 3, Javascript, Jquery and Bootstrap.


**Instructions**

- Clone this repository 
- Make sure you have any local server installed on your system like WAMP, LAMPP, XAMPP or MAMP based on your Operating System which comes up with an Apache server and MySQL. 
- Ensure that you are placing these files in the www directory within the wamp folder if you're using WAMP and the required path in all other cases. 
- Turn on the services and navigate to ` ` ` 127.0.0.1 (localhost)/Mini-Social Media/index.html ` ` `
- You are required to create two tables, one for storing the user details and the other for the posts manually in the Structured Query Language (SQL). 
- Just signup and login with the credentials, you can see your profile page dynamically created. 
- Don't forget to star this repository. 


**Database Schema**

![image](https://github.com/jamesgeorge007/Mini-Social-Media/blob/master/templates/assets/images/Database%20Schema/DB.JPG)

**Implemented features**

 
- User Authentication. 
- Sign-up feature. 
- Ability to post and view all the posts. 
- Logout feature. 
- Find Friends Feature. 
 
 
 
 **Still to be implemented**

  
* Notifications. 
* Followers. 
* Reasearching... 
  

**Status**

> Development stage.
